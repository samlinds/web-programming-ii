import fs from 'fs';
import zlib from 'zlib';

class lab03 {

  syncFileRead(filename) {
    var data = fs.readFileSync(filename);
    return data.toString();
  }

  asyncFileRead(filename, callback) {
   fs.readFile(filename, function (err, data) {
      if (err) return console.error(err);
      callback(data.toString());
    });
  }

  compressFileStream(input, output) {

    var stream = fs.createReadStream(input)
    .pipe(zlib.createGzip())
    .pipe(fs.createWriteStream(output));

    console.log("compress test");
    return stream;
  }

  decompressFileStream(input, output) {
    console.log("decompress test")
    var stream = fs.createReadStream(input)
    .pipe(zlib.createGunzip())
    .pipe(fs.createWriteStream(output));

    return stream;
  }

  listDirectoryContents(directory, callback) {
    var files = fs.readdir(directory, function (err, files) {
      if (err) return console.error(err);
      return callback(files);
    });

  }

}

export {lab03};
