// var bob = new Author({ name: 'Bob Smith' });
// bob.save(function (err) {  if (err) return handleError(err);
// //Bob now exists, so lets create a story
// var story = new Story({    title: "Bob goes sledding",
// // assign the _id from the our author Bob.
// // This ID is created by default!
//   author: bob._id   });
// story.save(function (err) {    if (err) return handleError(err);    // Bob now has his story  });});
//
// .findOne({ title: 'Bob goes sledding' })   //This populates the author id    //with actual author information!   .populate('author')    .exec(function (err, story) {     if (err) return handleError(err);     console.log('The author is %s',         story.author.name);     // prints "The author is Bob Smith"});
